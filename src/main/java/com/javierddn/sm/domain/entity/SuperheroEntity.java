package com.javierddn.sm.domain.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

@Data
public class SuperheroEntity {

    private final UUID id;
    private final String name;

    @JsonCreator
    public SuperheroEntity(@JsonProperty("id") final UUID id, @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }
}
