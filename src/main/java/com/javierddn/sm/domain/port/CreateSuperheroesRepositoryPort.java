package com.javierddn.sm.domain.port;

import com.javierddn.sm.domain.entity.SuperheroEntity;

public interface CreateSuperheroesRepositoryPort {

    SuperheroEntity create(SuperheroEntity superheroEntity);

}
