package com.javierddn.sm.domain.port;

import com.javierddn.sm.domain.entity.SuperheroEntity;

import java.util.List;

public interface GetSuperheroesServicePort {

    List<SuperheroEntity> get(SuperheroEntity superheroEntity);

}
